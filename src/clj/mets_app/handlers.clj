
(ns mets-app.handlers
  (:require [mets-app.load-data :as ld]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.data.json :as json]
            ))

(defn get-all-years [source]
  (sort (keys source)))

(defn handle-all-years []
  (let [all-years (get-all-years @ld/source-data)
       out-json (json/write-str all-years)
       ]
    {
     :headers {"Content-Type" "application/json"} 
     :body out-json
     }
    )
  )

(defn handle-year-data [year] 
  (let [selected-year (get @ld/source-data year)
        out-json (json/write-str selected-year)
        ]
    {
     :headers {"Content-Type" "application/json" }
     :body out-json
     }
    ))
