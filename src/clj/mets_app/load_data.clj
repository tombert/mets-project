(ns mets-app.load-data
  (:gen-class)
  (:require [clojure.data.csv :as csv]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.java.io :as io]))

(def source-data (atom {}))


; Lifted from https://github.com/clojure/data.csv/
(defn csv-data->maps [csv-data]
  (map zipmap
       (->> (first csv-data) ;; First row is the header
            (map keyword) ;; Drop if you want string keys instead
            repeat)
	  (rest csv-data)))

(defn load-source [csv-path]
  ; automatically closes the reader once it's out of scope. 
  (with-open [reader (io/reader csv-path)]
    (->> (csv/read-csv reader)
         (csv-data->maps)


         (map (fn [mapped-row]
              (let [
                    last_name (:last_name	 mapped-row)
                    first_name (:first_name	mapped-row)
                    player_id (:player_id	mapped-row)
                    year (:year	mapped-row)
                    xba (:xba mapped-row)
                    xslg (:xslg	mapped-row)
                    xwoba (:xwoba	mapped-row)
                    xobp (:xobp	mapped-row)
                    xiso (:xiso	mapped-row)
                    exit_velocity_avg (:exit_velocity_avg	mapped-row)
                    launch_angle_avg (:launch_angle_avg	mapped-row)
                    barrel_batted_rate (:barrel_batted_rate mapped-row)

                    ]
                
                (swap! source-data assoc-in [year exit_velocity_avg] mapped-row))))
         ; the csv reader is lazy.  We need to realize all side effects before the file closes
         (doall)
         )) 
  (println (str @source-data))
  nil)

