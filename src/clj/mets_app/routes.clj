(ns mets-app.routes
  (:require [clojure.java.io :as io]
            [compojure.core :refer [ANY GET PUT POST DELETE routes]]
            [compojure.route :refer [resources]]
            [mets-app.handlers :as h]

            [ring.util.response :refer [response]]))

(defn home-routes [endpoint]
  (routes
   (GET "/allyears" _ (h/handle-all-years))
   (GET "/yeardata/:year" [year] (h/handle-year-data year))
   (GET "/" _
     (-> "public/index.html"
         io/resource
         io/input-stream
         response
         (assoc :headers {"Content-Type" "text/html; charset=utf-8"})))
   (resources "/")))
