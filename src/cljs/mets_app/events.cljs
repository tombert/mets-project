(ns mets-app.events
  (:require [re-frame.core :as re-frame]
             [day8.re-frame.http-fx]

             [ajax.core :as ajax]
            [mets-app.db :as db]))

(re-frame/reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/reg-event-db
  :get-years-success
  (fn [db [_ result]]
   (assoc db :years result)
    )
  )
(re-frame/reg-event-db
  :get-year-data-success
  (fn [db [_ result]]
    (assoc db :year-data result)
    )
  )

(re-frame/reg-event-db
  :set-graph-obj
  (fn [db [_ g]]
    (assoc db :graphobj g)))
                  
(re-frame/reg-event-fx
  :get-year-data
  (fn [cofx [_ country]] ;; get the co-effects and destructure the event a
    {:http-xhrio {:uri (str "/yeardata/" country)
                  :method :get
                  :timeout 10000
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:get-year-data-success]
                  }
     :db (:db cofx)}))




(re-frame/reg-event-fx
  :set-selected-year 
  (fn [cofx [_ year]]
    {
     :dispatch   [:get-year-data year] 

     :db (assoc (:db cofx) :selected-year year)}
    )
  )


(re-frame/reg-event-fx
  :get-years
  (fn [cofx [_ item-id]] ;; get the co-effects and destructure the event a
    {:http-xhrio {:uri (str "/allyears")
                  :method :get
                  :timeout 10000
                  :response-format (ajax/json-response-format {:keywords? true})

                  :on-success [:get-years-success]
                  }
     :db (:db cofx)}))
  

(re-frame/reg-event-db 
  :initialize-db
  (fn [_ _]
    
    )
  )
