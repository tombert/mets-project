(ns mets-app.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [mets-app.events]
            [mets-app.subs]
            [mets-app.views :as views]
            [mets-app.config :as config]))

(enable-console-print!)

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn render []
  (re-frame/dispatch-sync [:initialize-db])
  (re-frame/dispatch [:get-years])

  (re-frame/dispatch 
    [:set-selected-year "2019"])

  (dev-setup)
  (mount-root))
