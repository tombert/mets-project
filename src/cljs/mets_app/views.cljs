(ns mets-app.views
  (:require [re-frame.core :as re-frame]
            [cljsjs.chartjs]
            [reagent.core :as reagent]

            ))

(enable-console-print!)

(def log (.-log js/console))  

(defn show-revenue-chart
  [comp-name graph-type data]
  (let [context (.getContext (.getElementById js/document comp-name) "2d")
        ;_ (println "Obj" (str data))
        chart-data {:type graph-type
                    :data data
                    }
        g (js/Chart. context (clj->js chart-data))
        ]
      
    (re-frame/dispatch [:set-graph-obj g])
    g
      
      ))


(defn update-revenue-chart
  [comp-name graph-type data]
  (let [g (re-frame/subscribe [:graphobj])
        gd (deref g)
        ]
    (println "data object: " (str data))

    (set! (.. gd -data) (clj->js []))
    (set! (.. gd -data) (clj->js data))
    ;(set! (.. js/window -location -search) "foo=bar"


    (.update gd)))


(defn rev-chartjs-component
  [comp-name graph-type data width height subscription cleanup] 
  (let [
        datasets (get data ::datasets)
        labels (get data :labels)
        ]
  (if (and (not (empty? datasets)) (not (empty? (get (first datasets) :data) )))
    (reagent/create-class
      {:component-did-mount #(show-revenue-chart comp-name graph-type data)
       :component-did-update (fn [arg...] 
                               (let [ctimedata (re-frame/subscribe [subscription])]
                                 (update-revenue-chart comp-name graph-type (cleanup @ctimedata))))
       :display-name        comp-name
       :reagent-render      (fn []
                              [:canvas {:id comp-name :width (str width) :height (str height)}])
       })
    [:div "nada"])
  
  ))


(defn transform-data [data]
  (let [
        data-vec (->> data (into [] ) (sort-by first))
        exit-vel-vec (->> data-vec (mapv (fn [[k v]]
                                           k
                                       
                                       )))
        launch-angle-avg (->> data-vec (mapv (fn [[k v]] 
                                               (:launch_angle_avg v)
                                               )))
        res {:labels exit-vel-vec
             ::datasets [{:data launch-angle-avg 
                          :label "Launch Angle Avg" }]

             }
        ]
    (log (clj->js res))
    res
    
    )
  )

(defn log [& args] (apply (.-log js/console) args))

(defn main-panel []
  (let [name (re-frame/subscribe [:name])
        years (re-frame/subscribe [:years])
        selected-year (re-frame/subscribe [:selected-year])
        ]
    (fn []
      [:div [:h1 "Stats viewer" ]
       [:select {:value @selected-year
                 :on-change (fn [i] 
                              (let [year (-> i .-target .-value)]
                                (re-frame/dispatch 
                                  [:set-selected-year year])
                                ))
                 
                 }
        (->> @years
             (map (fn [i]
                    [:option {:key i} i]
                    )))
        ]
       (let [
             year-data (re-frame/subscribe [:year-data])
             year-datad (deref year-data)
             transformed-data (transform-data year-datad)
             
             ]
        [:div "testing"]
        (log "Yo" (clj->js transformed-data))
       [rev-chartjs-component (str "yeardata" ) "bar" transformed-data 300 100 :year-data transform-data] 

       )
       [:div {:style {"text-align" "center"}} "Exit Velocity Average"]
       [:div [:img {:src "/construction.webp" :width "50px"}] "This page is under construction!"]
       ])))
