(ns mets-app.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 :name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
  :selected-year 
  (fn [db]
    (:year db)
    )
  )

(re-frame/reg-sub
 :years
 (fn [db]
   (:years db)))

(re-frame/reg-sub
  :year-data
  (fn [db]
    (:year-data db)))

(re-frame/reg-sub
  :graphobj 
  (fn [db]
    (:graphobj db)
    )
  )

