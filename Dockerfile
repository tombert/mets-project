from clojure

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN lein uberjar

EXPOSE 10555

CMD ["java", "-Xmx2g", "-jar", "target/mets_app.jar"]

