# mets_app

To run, run the script `build_and_run.sh`.  Please have docker installed. 

Once run, go to `localhost:10555` in the browser. 

# Open Source

This application uses the following open source libraries (and whatever dependencies that they pull in recursively). 

## Clojure Chestnut: 

https://github.com/plexus/chestnut
Eclipse Public License 1.0


## Re-frame: 

https://github.com/day8/re-frame
MIT License

## http-kit

https://github.com/http-kit/http-kit
Apache License Version 2


## data.json

https://github.com/clojure/data.json
Eclipse Public License - v 1.0

## data.csv
https://github.com/clojure/data.csv
Eclipse Public License - v 1.0
