(ns mets-app.test-runner
  (:require
   [doo.runner :refer-macros [doo-tests]]
   [mets-app.core-test]
   [mets-app.common-test]))

(enable-console-print!)

(doo-tests 'mets-app.core-test
           'mets-app.common-test)
